package packet_meteo;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class Application {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				MainFrame mainframe = new MainFrame("App M�t�o");
				mainframe.setResizable(false);
				mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				mainframe.pack();
				mainframe.setLocationRelativeTo(null);
				mainframe.setVisible(true);
			}
		});
	}
}
