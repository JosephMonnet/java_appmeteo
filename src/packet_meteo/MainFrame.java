package packet_meteo;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import packet_meteo.utilities.Alert;
import packet_meteo_models.CurrentWeather;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -7679725155820384766L;

	private static final String GENERIC_ERROR_MESSAGE = "Oups une erreur est survenue. Veuillez r�essayer.";
	private static final String INTERNET_CONNECTIVITY_ERROR_MESSAGE = "Veuillez v�rifier que vous �tes connect� � internet.";
	
	private static final Color BLUE_COLOR = Color.decode("#8EA2C6");
	private static final Color WHITE_COLOR = Color.WHITE;
	private static final Font DEFAULT_FONT = new Font("San Fransisco", Font.PLAIN, 24);
	private static final Color LIGHT_COLOR = new Color(255, 255, 255, 150);
	
	private JLabel locationLabel;
	private JLabel timeLabel;
	private JLabel temperatureLabel;
	private JPanel otherInfosPanel;
	private JLabel humidityLabel;
	private JLabel humidityValue;
	private JLabel windLabel;
	private JLabel windValue;
	private JLabel summaryLabel;
	
	private CurrentWeather currentWeather;
	
	public MainFrame(String title) {
		super(title);
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.setBorder(BorderFactory.createEmptyBorder(25,25,25,25));
		contentPane.setBackground(BLUE_COLOR);
		
		locationLabel = new JLabel("Bordeaux, Fr", SwingConstants.CENTER);
		locationLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		locationLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));
		locationLabel.setForeground(WHITE_COLOR);
		locationLabel.setFont(DEFAULT_FONT);
		
		timeLabel = new JLabel("...", SwingConstants.CENTER);
		timeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		timeLabel.setForeground(new Color(255, 255, 255, 150));
		timeLabel.setFont(DEFAULT_FONT.deriveFont(15f));	
		
		temperatureLabel = new JLabel("--", SwingConstants.CENTER);
		temperatureLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		temperatureLabel.setForeground(WHITE_COLOR);
		temperatureLabel.setFont(DEFAULT_FONT.deriveFont(90f));
		
		otherInfosPanel = new JPanel(new GridLayout(2, 2));
		otherInfosPanel.setBackground(BLUE_COLOR);
		
		humidityLabel = new JLabel("Humidit�".toUpperCase(), SwingConstants.CENTER);
		humidityLabel.setForeground(LIGHT_COLOR);
		humidityLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		humidityValue = new JLabel("--", SwingConstants.CENTER);
		humidityValue.setForeground(WHITE_COLOR);
		humidityValue.setAlignmentX(Component.CENTER_ALIGNMENT);
		humidityValue.setFont(DEFAULT_FONT);
		
		windLabel = new JLabel("Vent".toUpperCase(), SwingConstants.CENTER);
		windLabel.setForeground(LIGHT_COLOR);
		windLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		windValue = new JLabel("--", SwingConstants.CENTER);
		windValue.setForeground(WHITE_COLOR);
		windValue.setAlignmentX(Component.CENTER_ALIGNMENT);
		windValue.setFont(DEFAULT_FONT);
		
		otherInfosPanel.add(humidityLabel);
		otherInfosPanel.add(windLabel);
		otherInfosPanel.add(humidityValue);
		otherInfosPanel.add(windValue);
		
		summaryLabel = new JLabel("R�cup�ration de la temp�rature actuelle...", SwingConstants.CENTER);
		summaryLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		summaryLabel.setForeground(WHITE_COLOR);
		summaryLabel.setBorder(BorderFactory.createEmptyBorder(30, 0, 0, 0));
		summaryLabel.setFont(DEFAULT_FONT.deriveFont(14f));
		
		contentPane.add(locationLabel);
		contentPane.add(timeLabel);
		contentPane.add(temperatureLabel);
		contentPane.add(otherInfosPanel);
		contentPane.add(summaryLabel);
		
		setContentPane(contentPane);

		String apiKey = "a75bb0133d7e8574713363ed4323ea8c";
		String city = "Bordeaux";
		String forecastUrl = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + apiKey +"&units=metric&lang=fr";

		OkHttpClient client = new OkHttpClient();
		Request request = new Request.Builder().url(forecastUrl).build();
		Call call = client.newCall(request);
		call.enqueue(new Callback() {

			@Override
			public void onResponse(Call call, Response response) {
				try(ResponseBody body = response.body()) {
					if (response.isSuccessful()) {
						String jsonData = body.string();
						currentWeather = getCurrentWeatherDetails(jsonData);
						
						try {
							Thread.sleep(2500);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						EventQueue.invokeLater(new Runnable() {
							@Override
							public void run() {
								updateScreen();	
							}
						});
					} else {
						Alert.error(MainFrame.this, GENERIC_ERROR_MESSAGE);
					}
				} catch (ParseException | IOException e) {
					Alert.error(MainFrame.this, GENERIC_ERROR_MESSAGE);
				}
			}

			@Override
			public void onFailure(Call call, IOException e) {
				Alert.error(MainFrame.this, INTERNET_CONNECTIVITY_ERROR_MESSAGE);
			}
		});
	}

	protected void updateScreen() {
		timeLabel.setText("Il est " + currentWeather.getFormattedTime() + " et la temp�rature actuelle est de :");
		temperatureLabel.setText(currentWeather.getTemperature() + " C�");
		humidityValue.setText(currentWeather.getHumidity() + " %");
		windValue.setText(currentWeather.getSpeed_wind() + " km/h");
		summaryLabel.setText(currentWeather.getDescription() + "");
		
	}

	private CurrentWeather getCurrentWeatherDetails(String jsonData) throws ParseException {
		CurrentWeather currentWeather = new CurrentWeather();
		JSONObject forecast = (JSONObject) JSONValue.parseWithException(jsonData);
		JSONObject main = (JSONObject) forecast.get("main");
		JSONObject wind = (JSONObject) forecast.get("wind");
		JSONArray weather = (JSONArray) forecast.get("weather");
		JSONObject weather_0 = (JSONObject) weather.get(0);
		currentWeather.setName((String) forecast.get("name"));
		currentWeather.setTemperature((double) main.get("temp"));
		currentWeather.setTime((long) forecast.get("dt"));
		currentWeather.setHumidity(Double.parseDouble(main.get("humidity") + ""));
		currentWeather.setSpeed_wind(Double.parseDouble(wind.get("speed") + ""));
		currentWeather.setDescription((String) weather_0.get("description"));
		return currentWeather;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(500, 500);
	}

	@Override
	public Dimension getMinimumSize() {
		return getPreferredSize();
	}

	@Override
	public Dimension getMaximumSize() {
		return getPreferredSize();
	}

}
