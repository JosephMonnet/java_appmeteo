package packet_meteo_models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentWeather {
	private String name;
	private double temperature;
	private long time;
	private long timezone;
	private double humidity;
	private double speed_wind;
	private String description;
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getTemperature() {
		return (int) Math.round(temperature);
	}
	
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	public long getTime() {
		return time;
	}
	
	public String getFormattedTime() {
		Date date = new Date(getTime()*1000L);
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		String timeString = formatter.format(date);
		return timeString;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	
	public String setTimezone() {
		long gmt = timezone;
		String timezone_str = String.valueOf(gmt);
		return timezone_str;

	}
	
	public String getTimezone() {
		long gmt = timezone;
		String timezone_str = String.valueOf(gmt);
		return timezone_str;

	}
	
	public int getHumidity() {
		return (int) Math.round(humidity);
	}
	
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	
	public int getSpeed_wind() {
		return (int) Math.round(speed_wind * 3.6);
	}
	
	public void setSpeed_wind(double speed_wind) {
		this.speed_wind = speed_wind;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
